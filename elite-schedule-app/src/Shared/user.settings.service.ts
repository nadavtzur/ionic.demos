import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite';
import { SqlStorage } from './shared';

const win: any = window;

@Injectable()
export class UserSettings {
  //storage = new Storage(SqlStorage);
  //public db: SQLite;
  public sql: SqlStorage;

  constructor(public events: Events, public storage: Storage,  public db: SQLite) {
    if (win.sqlitePlugin) {
      this.sql = new SqlStorage(this.db);
      alert("has win.sqlitePlugin")
    } else {
      console.warn('SQLite plugin not installed. Falling back to regular Ionic Storage.');
    }
  }

  favoriteTeam(team, tournamentId, tournamentName) {
    let item = { team: team, tournamentId: tournamentId, tournamentName: tournamentName };

    if (this.sql){
      //alert("has sql")
      this.sql.set(team.id.toString(), JSON.stringify(item)).then(data => {
        this.events.publish('favorites:changed');
      });
    } else {
      //alert("has storage")
      return new Promise(resolve => {
        this.storage.set(team.id.toString(), JSON.stringify(item)).then(() => {
          this.events.publish('favorites:changed');
          resolve();
        });
      });
    }
  }

  unfavoriteTeam(team) {
    if (this.sql){
      this.sql.remove(team.id.toString()).then(data => {
        this.events.publish('favorites:changed');
      });
    } else {
      return new Promise(resolve => {
        this.storage.remove(team.id.toString()).then(() => {
          this.events.publish('favorites:changed');
          resolve();
        });
      });
    }
  }

  isFavoriteTeam(teamId): Promise<boolean> {
    if (this.sql){
      return this.sql.get(teamId.toString()).then(value => value ? true : false);
    } else {
      return new Promise(resolve => resolve(this.storage.get(teamId.toString()).then(value => value ? true : false)));
    }
  }

  getAllFavorites(): Promise<any[]> {
    if (this.sql){
      return this.sql.getAll();
    } else {
      return new Promise(resolve => {
        let results = [];
        this.storage.forEach(data => {
          results.push(JSON.parse(data));
        });
        return resolve(results);
      });
    }
  }

  initStorage(): Promise<any> {
    if (this.sql){
      //alert("this.sql")
      return this.sql.initializeDatabase();
    } else {
      return new Promise(resolve => resolve());
    }
  }
}
