import { Component, ViewChild } from '@angular/core';
import {Events, LoadingController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyTeamsPage } from "../pages/my-teams/my-teams";
import {TournamentsPage} from "../pages/tournaments/tournaments";
import {UserSettings} from "../Shared/user.settings.service";
import {EliteApi} from "../Shared/elite-api.service";
import {BarcodeScannerPage} from "../pages/barcode-scanner/barcode-scanner";


@Component({
  templateUrl: 'app.html'

})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = MyTeamsPage;
  favoriteTeams: any;

  constructor(
    public events: Events,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loadingController: LoadingController,
    public platform: Platform,
    public eliteApi: EliteApi,
    public userSettings: UserSettings) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //alert("before initStorage()")
      this.userSettings.initStorage().then(() => {
        //alert("after initStorage()")
        this.rootPage = MyTeamsPage;
        this.refreshFavorites();
        this.events.subscribe('favorites:changed', () => this.refreshFavorites());
      });


    });
  }

  goHome() {

    this.nav.push(MyTeamsPage);

  }

  goToTournaments() {

    this.nav.push(TournamentsPage);

  }

  refreshFavorites(){
    this.userSettings.getAllFavorites().then(favs => this.favoriteTeams = favs);
    //this.favoriteTeams = this.userSettings.getAllFavorites();
  }

  goToBarcodeScanner(){
    this.nav.push(BarcodeScannerPage);
  }

}
