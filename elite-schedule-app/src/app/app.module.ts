import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MyTeamsPageModule } from '../pages/my-teams/my-teams.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {TournamentsPageModule} from "../pages/tournaments/tournaments.module";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import {TeamsPageModule} from "../pages/teams/teams.module";
import {GamePageModule} from "../pages/game/game.module";
import {TeamHomePageModule} from "../pages/team-home/team-home.module";
import {EliteApi, UserSettings} from "../Shared/shared";
import { HttpClientModule, HttpClient} from '@angular/common/http'
import { IonicStorageModule} from '@ionic/storage';
import {MapPageModule} from "../pages/map/map.module";
import {BarcodeScannerPageModule} from "../pages/barcode-scanner/barcode-scanner.module";
import {SQLite} from "@ionic-native/sqlite";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
  ],
  imports: [
    BrowserModule,
    MyTeamsPageModule,
    TournamentsPageModule,
    TeamsPageModule,
    GamePageModule,
    BarcodeScannerPageModule,
    MapPageModule,
    IonicStorageModule,
    TeamHomePageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClientModule,
    HttpClient,
    EliteApi,
    SQLite,
    Storage,
    UserSettings,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
