import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamHomePage } from './team-home';
import { TeamDetailPage } from "./team-detail/team-detail";
import { StandingsPage } from "./standings/standings";


@NgModule({
  declarations: [
    TeamHomePage,
    TeamDetailPage,
    StandingsPage
  ],
  entryComponents: [
    TeamDetailPage,
    StandingsPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamHomePage),
  ],
})
export class TeamHomePageModule {}
