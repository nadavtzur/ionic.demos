import { Component } from '@angular/core';
import {  NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import * as _ from 'lodash';
import { EliteApi, UserSettings } from "../../../Shared/shared";
import {GamePage} from "../../game/game";
import * as moment from 'moment';

@Component({
  selector: 'page-team-detail',
  templateUrl: 'team-detail.html',
})
export class TeamDetailPage {

  allGames: any[];
  dateFilter: string;
  games: any[];
  isFollowing : any;
  team: any;
  teamStanding: any;
  private tourneyData: any;
  useDateFilter = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private eliteApi: EliteApi,
              private alertController: AlertController,
              private toastController: ToastController,
              private userSettings: UserSettings) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamDetailPage');
    this.team = this.navParams.data;
    this.tourneyData = this.eliteApi.getCurrentTourney();
    this.games = _.chain(this.tourneyData.games)
                 .filter(g => g.team1Id === this.team.id || g.team2Id === this.team.id)
                 .map( g => {
                   let isTeam1 = ( g.team1Id === this.team.id);
                   let opponentName = isTeam1 ? g.team2 : g.team1;
                   let scorePlay = this.getScoreDisplay(isTeam1, g.team1Score, g.team2Score);
                   return {
                     gameId:g.id,
                     oponnent: opponentName,
                     time: Date.parse(g.time),
                     location: g.location,
                     locationUrl: g.locationUrl,
                     scorePlay: scorePlay,
                     homeAway: (isTeam1 ? "vs." : "at")
                   }
                 }).value();
    this.allGames = this.games;
    //console.log('this.tourneyData.standings', this.tourneyData.standings)
    this.teamStanding = _.find(this.tourneyData.standings, {'teamId': this.team.id});
    this.userSettings.isFavoriteTeam(this.team.id).then(value => this.isFollowing = value);
    //console.log(this.games);

  }

  getScoreDisplay(isTeam1, team1Score, team2Score){

    if(team1Score && team2Score){
      var teamScore = (isTeam1 ? team1Score: team2Score);
      var opponentScore = (isTeam1 ? team2Score: team1Score);
      var winIndicator = teamScore > opponentScore ? "W: " : "L ";
      return winIndicator + teamScore + "-" + opponentScore;
    }
    else{
      return "";
    }

  }

  gameClicked($event, game){

    let sourceGame = this.tourneyData.games.find(g=>g.id === game.gameId);
    //console.log(sourceGame);
    this.navCtrl.parent.parent.push(GamePage, sourceGame);

  }

  getScoreWorL(game){
     return game.scorePlay ? game.scorePlay[0] : '';
  }

  getScoreDisplayBadgeClass(game){
    return game.scorePlay.indexOf('W:') === 0 ? 'badge-primary' : 'badge-danger';

  }

  dateChanged(){
    if (this.useDateFilter) {
      this.games = _.filter(this.allGames, g => moment(g.time).isSame(this.dateFilter, 'day'));
    } else {
      this.games = this.allGames;
    }
  }

  toggleFollow(){
    if (this.isFollowing) {
      let confirm = this.alertController.create({
        title: 'Unfollow?',
        message: 'Are you sure you want to unfollow?',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              this.isFollowing = false;
               this.userSettings.unfavoriteTeam(this.team);

              let toast = this.toastController.create({
                message: 'You have unfollowed this team.',
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
            }
          },
          { text: 'No' }
        ]
      });
      confirm.present();
    } else {
      this.isFollowing = true;
      this.userSettings.favoriteTeam(
        this.team,
        this.tourneyData.tournament.id,
        this.tourneyData.tournament.name);

    }
  }

  refreshAll(refresher){
    this.eliteApi.refreshCurrentTourney().subscribe(() => {
      refresher.complete();
      this.ionViewDidLoad();
    });
  }

}
