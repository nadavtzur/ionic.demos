import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import {TeamsPage} from "../teams/teams";
import { EliteApi } from "../../Shared/shared";


@IonicPage()
@Component({
  selector: 'page-tournaments',
  templateUrl: 'tournaments.html',
})
export class TournamentsPage {

  tournaments:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private eliteApi:EliteApi,
              private loadingController:LoadingController) {
  }


  itemTapped($event, item){
    this.navCtrl.push(TeamsPage, item);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TournamentsPage');
    let loader = this.loadingController.create({
      content: 'Getting tournaments...',
      //spinner: 'dots'
    });

    loader.present().then(()=>{

      this.eliteApi.getTournaments().then(data => this.tournaments = data);
      loader.dismiss();

    })
  }

  ionViewDidUnload() {
    console.log('ionViewDidUnload TournamentsPage');
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave TournamentsPage');
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter TournamentsPage');
  }


}
