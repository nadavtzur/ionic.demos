import { Component } from '@angular/core';
import { IonicPage,  NavParams } from 'ionic-angular';
import {EliteApi} from "../../Shared/elite-api.service";


declare var window: any;
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',

})
export class MapPage {

  map: any;

  constructor(public navParams: NavParams, public eliteApi: EliteApi) {

  }

  ionViewDidLoad(){
    let games = this.navParams.data;
    let tourneyData = this.eliteApi.getCurrentTourney();
    let location = tourneyData.locations[games.locationId];

    if(location){
      this.map = {
        lat: location.latitude,
        lng: location.longitude,
        zoom: 12,
        markerLabel: games.location
      };
    }

  }

  getDirections() {
    window.location = `geo:${this.map.lat},${this.map.lng};u=35`;
  }

}
