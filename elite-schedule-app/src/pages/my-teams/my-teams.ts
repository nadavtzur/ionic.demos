import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import {TournamentsPage} from "../tournaments/tournaments";
import { EliteApi } from "../../Shared/shared";
import {TeamHomePage} from "../team-home/team-home";
import {UserSettings} from "../../Shared/user.settings.service";


@IonicPage()
@Component({
  selector: 'page-my-teams',
  templateUrl: 'my-teams.html',
})
export class MyTeamsPage {

   favorites :any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingController: LoadingController,
              private eliteApi: EliteApi,
              public userSettings: UserSettings) {
  }

  ionViewDidLoad() {

  }

  favoriteTapped($event, favorite){
    let loader = this.loadingController.create({
      content: 'Getting data...',
      dismissOnPageChange: true
    });
    loader.present().then(()=>{

      this.eliteApi.getTournamentData(favorite.tournamentId)
        .subscribe(t => this.navCtrl.push(TeamHomePage, favorite.team));

      loader.dismiss();
    });

  }

  goToTournaments(){
    this.navCtrl.push(TournamentsPage)
  }

  ionViewDidEnter(){
    this.userSettings.getAllFavorites().then(favs => this.favorites = favs);
  }

}
