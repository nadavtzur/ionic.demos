import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'


@IonicPage()
@Component({
  selector: 'page-barcode-scanner',
  templateUrl: 'barcode-scanner.html',
})
export class BarcodeScannerPage {

  results: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private barcodeScanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BarcodeScannerPage');
  }

  scan(){
    this.barcodeScanner.scan().then((barcodeData) => {
      this.results = barcodeData;
    },(err) => {
      alert(`Error scanning: ${err}`);
    })
  }

  reset(){
    this.results = null;
  }

  lookup(){
    window.open(`http://www.upcindex.com/${this.results.text}`, '_system');
  }

}
