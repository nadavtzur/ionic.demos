import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'
import 'rxjs';
import { Observable } from 'rxjs/Observable'


@Injectable()
export class AuthenticationServiceProvider {

  private authenticationData: any = {}
  private isAuthenticated:boolean = false;
  private token: string;
  private tokenType:string;
  private username: string;
  private password: string;
  private authUrl: string = 'mtech/Sonar/Installer/api/Token';//'https://api.mtech-systems.com/Sonar/Installer/api/Token';//



  constructor(public http: HttpClient) {
  }

  login(username:string, password:string): Observable<any>{

    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json-patch+json');
    headers.set('Accept', 'text/plain');

    let body = {
      "userName": "root",
      "password": "qwerty"
    }

   /* let body = {
      "userName": `${username}`,
      "password": `${password}`
    }*/

    console.log(body)

    return this.http.post(this.authUrl,body,{headers: headers}).map((res: Response) => {

      this.authenticationData = res;
      this.token = this.authenticationData.access_token;
      this.tokenType = this.authenticationData.token_type;
      this.username = username;
      this.password = password;
      this.isAuthenticated = true;
      window.localStorage.setItem('token', this.token);

      return Observable.of(true);

    }).catch((error: any) => {
      this.isAuthenticated = false;
      console.log(error)
      return Observable.of(false);
    });

  }

  createHeaderForGet(){
    let header = new HttpHeaders()
      .set('authorization', 'bearer ' + window.localStorage.getItem('token'));
    return header;
  }

  createHeaderForPost(){
    let header  = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('authorization', 'bearer ' + window.localStorage.getItem('token'));
    return header;
  }

  createHeaderForDelete(){
    let header  = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('authorization', 'bearer ' + window.localStorage.getItem('token'));
    return header;
  }

}
