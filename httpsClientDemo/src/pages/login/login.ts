import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController} from 'ionic-angular';
import {AuthenticationServiceProvider} from '../../providers/authentication-service/authentication-service'
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string;
  password: string;

  constructor(public navCtrl: NavController,
              public authService:AuthenticationServiceProvider,
              private loadingController: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    if(this.username && this.password){

      let loader = this.loadingController.create({
        content: 'Authenticating...',
      });

      loader.present().then(()=> {
        this.authService.login(this.username,this.password).subscribe(data=>{

          if(data){
             window.localStorage.setItem('username', this.username);
             window.localStorage.setItem('password', this.password);
            this.navCtrl.setRoot(HomePage);
          }
          else{
            //alert
          }
        })
          loader.dismiss();
        });
    }
    else{
      //alert or something
    }
  }
}
